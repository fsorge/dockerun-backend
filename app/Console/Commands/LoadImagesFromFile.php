<?php

namespace App\Console\Commands;

use App\Models\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LoadImagesFromFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loads images.json to table on database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->ask('Path to images.json');

        $this->info("Loading file: " . $path);

        $fileContent = file_get_contents($path);
        if ($fileContent === false) {
            die("Can't load file");
        }
        $imagesJson = json_decode(file_get_contents($path), true)['images'];

        foreach ($imagesJson as $image) {
            $imageModel = new Image();
            $imageModel->fill($image);
            $imageModel->save();

            $this->info($image['name'] . ' saved into db');
        }

        $this->info("Batch finished");

        return 0;
    }
}
