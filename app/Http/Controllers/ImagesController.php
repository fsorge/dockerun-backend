<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Image;
use Illuminate\Http\JsonResponse;

class ImagesController extends Controller
{
    public function suggest(Request $request): JsonResponse {
        $request->validate([
            'input' => ['required', 'min:3'],
        ]);

        $input = $request->get('input');

        Log::info($input);

        $res = Image::where('name', 'LIKE', "%$input%")->orWhere('commonName', 'LIKE', "%$input%")->orWhereJsonContains('keywords', $input)->orderBy('commonName')->limit(5)->get();

        return response()->json($res);
    }
}
