<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'commonName',
        'description',
        'keywords',
        'versions',
        'link',
        'color',
        'envs',
        'ports',
        'volumes',
        'links',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'keywords' => 'array',
        'versions' => 'array',
        'envs' => 'array',
        'ports' => 'array',
        'volumes' => 'array',
        'links' => 'array',
    ];

    protected $appends = ['image'];

    public function getImageAttribute() {
        return config('app.static_assets_url') . "/images-logo/". str_replace('/', '-', $this->name) .".png";
    }
}
