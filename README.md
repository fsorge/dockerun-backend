<img alt="Quality Gate Status" src="https://sonarcloud.io/api/project_badges/measure?project=dockerun-backend&metric=alert_status"/>

<img alt="Maintainability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=dockerun-backend&metric=sqale_rating"/>

<img alt="Reliability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=dockerun-backend&metric=reliability_rating"/>

<img alt="Security Rating" src="https://sonarcloud.io/api/project_badges/measure?project=dockerun-backend&metric=security_rating"/>

<img alt="Bugs" src="https://sonarcloud.io/api/project_badges/measure?project=dockerun-backend&metric=bugs"/>

<img alt="Vulnerabilities" src="https://sonarcloud.io/api/project_badges/measure?project=dockerun-backend&metric=vulnerabilities"/>

<img alt="Pipeline" src="https://img.shields.io/gitlab/pipeline/fsorge/dockerun-backend/main"/>


# Dockerun backend

Dockerun backend provides images suggestion retrieved from a database, allowing me to insert new Docker images into Dockerun without having to deploy a new version each time. The old images.json is still available on Dockerun frontend and will be used in case of this API server being down or unreachable.


## Getting started

### Develop

1. Run `composer install`
2. Copy the `.env.example` into a new file `.env` and fill properties
3. Run `php artisan key:generate`
4. Run `php artisan migrate` to generate table structures
5. Start the live server `php artisan serve`
6. Start doing experiments!
